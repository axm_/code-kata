const readline = require('readline')

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

rl.question('Favorite color ? ', (answer) => {
  rl.write(`Your favorite color is: ${answer}`)
  rl.write('\n')
  rl.close()
})
